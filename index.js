import { createStore, applyMiddleware } from 'redux'
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import 'whatwg-fetch';
import rootReducer from './reducers/RootReducer';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { compose } from 'redux';

const createStoreWithMiddleware = compose (
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore);

const store = createStoreWithMiddleware(rootReducer);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('drive-container')
);