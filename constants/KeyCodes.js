export const ENTER_KEY_CODE = 13;
export const A_KEY_CODE = 65;
export const LEFT_ARROW_KEY_CODE = 37;
export const RIGHT_ARROW_KEY_CODE = 39;
export const TOP_ARROW_KEY_CODE = 38;
export const BOTTOM_ARROW_KEY_CODE = 40;
export const ESC_KEY_CODE = 27;
export const DELETE_KEY_CODE = 46;