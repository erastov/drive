import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import FilesTableHead from './blocks/table/FilesTableHead';
import FileItemRow from './blocks/table/FileItemRow';
import FileItemGrid from './blocks/grid/FileItemGrid';
import Footer from './Footer';
import * as constants from '../constants';
import * as KeyCodes from '../constants/KeyCodes';

class FilesGrid extends Component {

    componentDidMount() {
        window.addEventListener("keydown", event => {
            // e.shiftKey
            this.handleAPress(event);
            this.handleEnterPress(event);
            this.handleDeletePress(event);
            this.handleEscPress(event);
            this.handleArrowPress(event);
        }, false);
    }

    handleAPress(event) {
        if (event.ctrlKey && event.keyCode === KeyCodes.A_KEY_CODE && this.props.mainInfo.isFileFocus) {
            const files = this.props.mainInfo.files;
            this.props.driveActions.setSelectedIds(Object.keys(files));
            event.preventDefault();
        }
    }

    handleEnterPress(event) {
        if(event.keyCode === KeyCodes.ENTER_KEY_CODE) {
            if (this.props.mainInfo.selectedIds.length === 1) {
                const files = this.props.mainInfo.files;
                const fileId = this.props.mainInfo.selectedIds.shift();
                const file = files[fileId];
                if (file.isFolder) {
                    this.props.driveActions.loadFolderData(file.id);
                }
            }
        }
    }

    handleDeletePress(event) {
        if (event.keyCode === KeyCodes.DELETE_KEY_CODE) {
            this.deleteSelectedFiles(event);
        }
    }

    handleEscPress(event) {
        if (event.keyCode === KeyCodes.ESC_KEY_CODE) {
            this.props.driveActions.closeAllModalsAndMenu();
        }
    }

    handleArrowPress(event) {
        const arrowKeys = [KeyCodes.LEFT_ARROW_KEY_CODE, KeyCodes.TOP_ARROW_KEY_CODE, KeyCodes.RIGHT_ARROW_KEY_CODE, KeyCodes.BOTTOM_ARROW_KEY_CODE];
        if (arrowKeys.indexOf(event.keyCode) >= 0 && this.isHasSelectedFiles()) {
            event.preventDefault();

            const files = this.props.mainInfo.files;
            let filesKeys = Object.keys(files);
            const lastSelectedKeyIndex = filesKeys.indexOf(this.props.mainInfo.lastSelectedFileId);

            if (lastSelectedKeyIndex >= 0) {
                if (this.props.mainInfo.viewType === 'grid') {
                    let topIndex = lastSelectedKeyIndex - constants.GRID_COUNT;
                    let bottomIndex = lastSelectedKeyIndex + constants.GRID_COUNT;

                    const lastSelectedFileId = filesKeys[lastSelectedKeyIndex];
                    const nextTopSelectedFileId = filesKeys[topIndex];
                    const nextBottomSelectedFileId = filesKeys[bottomIndex];

                    let folderNumber = 0;
                    for (let i in files) {
                        if (files.hasOwnProperty(i) && files[i].isFolder === true) {
                            folderNumber++;
                        }
                    }

                    const gridShift = folderNumber % constants.GRID_COUNT;

                    if (nextTopSelectedFileId && files[lastSelectedFileId].isFolder != files[nextTopSelectedFileId].isFolder) {
                        topIndex = topIndex - ((lastSelectedKeyIndex - folderNumber) < gridShift ?  - gridShift : gridShift);
                    }

                    if (nextBottomSelectedFileId && files[lastSelectedFileId].isFolder != files[nextBottomSelectedFileId].isFolder) {
                        bottomIndex = bottomIndex + ((folderNumber - lastSelectedKeyIndex) > gridShift ? gridShift : - gridShift);
                    }

                    // left
                    if (event.keyCode === KeyCodes.LEFT_ARROW_KEY_CODE && lastSelectedKeyIndex > 0) {
                        if (event.shiftKey) {

                        } else {
                            this.props.driveActions.selectFile(filesKeys[lastSelectedKeyIndex - 1])
                        }
                    }

                    // top
                    if (event.keyCode === KeyCodes.TOP_ARROW_KEY_CODE && lastSelectedKeyIndex >= constants.GRID_COUNT) {
                        if (event.shiftKey) {

                        } else  {
                            this.props.driveActions.selectFile(filesKeys[topIndex])
                        }
                    }

                    // right
                    if (event.keyCode === KeyCodes.RIGHT_ARROW_KEY_CODE && lastSelectedKeyIndex < (filesKeys.length - 1)) {
                        if (event.shiftKey) {

                        } else {
                            this.props.driveActions.selectFile(filesKeys[lastSelectedKeyIndex + 1])
                        }
                    }

                    // bottom
                    if (event.keyCode === KeyCodes.BOTTOM_ARROW_KEY_CODE && lastSelectedKeyIndex < (filesKeys.length - constants.GRID_COUNT)) {
                        if (event.shiftKey) {

                        } else {
                            this.props.driveActions.selectFile(filesKeys[bottomIndex])
                        }
                    }
                } else {
                    // top
                    if (event.keyCode === KeyCodes.TOP_ARROW_KEY_CODE && lastSelectedKeyIndex > 0) {
                        if (event.shiftKey) {

                        } else {
                            this.props.driveActions.selectFile(filesKeys[lastSelectedKeyIndex - 1])
                        }
                    }

                    // bottom
                    if (event.keyCode === KeyCodes.BOTTOM_ARROW_KEY_CODE && lastSelectedKeyIndex < (filesKeys.length - 1)) {
                        if (event.shiftKey) {

                        } else {
                            this.props.driveActions.selectFile(filesKeys[lastSelectedKeyIndex + 1])
                        }
                    }
                }
            }
        }
    }

    deleteSelectedFiles = (e) => {
        e.preventDefault();
        if (this.isHasSelectedFiles()) {
            let data = new FormData();
            this.props.mainInfo.selectedIds.forEach(function(id) {
                data.append('ids[]', id);
            });

            this.props.driveActions.deleteSelectedFiles(data);
        }
    };

    isHasSelectedFiles() {
        return this.props.mainInfo.selectedIds.length > 0;
    }

    prepareFiles(files) {
        let filesArr = [];
        let isDirLastFile = false;
        for (var i in files) {
            if (files.hasOwnProperty(i)) {
                files[i]['isFolderLastFile'] = isDirLastFile;
                isDirLastFile = files[i]['isFolder'];
                filesArr.push(files[i]);
            }
        }
        return filesArr;
    }

    selectFileByClick = (e, fileId) => {
        e.preventDefault();
        const that = this;
        if (e.ctrlKey) {
            if (this.props.mainInfo.selectedIds.indexOf(fileId) == -1) {
                this.props.driveActions.addFileToSelected(fileId);
            } else {
                this.props.driveActions.removeFileFromSelected(fileId);
            }
        } else if(e.shiftKey) {
            let keyArray = Object.keys(this.props.mainInfo.files),
                indexOfLastSelected = keyArray.indexOf(this.props.mainInfo.lastSelectedFileId),
                indexOfClicked = keyArray.indexOf(fileId),
                selected;
            if (indexOfLastSelected != indexOfClicked) {
                if (indexOfLastSelected > indexOfClicked) {
                    selected = keyArray.splice(indexOfClicked, indexOfLastSelected - indexOfClicked + 1);
                } else {
                    selected = keyArray.splice(indexOfLastSelected, indexOfClicked - indexOfLastSelected + 1);
                }
                this.props.driveActions.setSelectedIds(selected);
            }
        } else {
            this.props.driveActions.selectFile(fileId);
        }
    };

    leftClickOnFilesContainer = e => {
        if (e.target.getAttribute('id') == 'filesContainer') {
            this.props.driveActions.filesOutClick();
            e.preventDefault();
        }
    };

    rightClickOnFilesContainer = e => {
        if (e.target.getAttribute('id') == 'filesContainer') {
            const coords = $('#drive-container').offset();
            this.props.driveActions.setSelectedIds([]);
            this.props.driveActions.toggleFileMenu(true, e.clientX - coords.left, e.clientY - coords.top);
            e.preventDefault();
        }
    };

    onDoubleClick = (e, file) => {
        e.preventDefault();
        if (file.isFolder) {
            this.props.driveActions.loadFolderData(file.id);
        }
    };

    rightClickFile = (e, fileId) => {
        const coords = $('#filesContainer').offset();
        if (this.props.mainInfo.selectedIds.indexOf(fileId) == -1) {
            this.props.driveActions.selectFile(fileId);
        }
        this.props.driveActions.toggleFileMenu(true, e.clientX - coords.left, e.clientY - coords.top);
        e.preventDefault();
    };

    render() {
        const files = this.prepareFiles(this.props.mainInfo.files);
        const isTableView = this.props.mainInfo.viewType === 'table';
        const ItemComponent = isTableView ? FileItemRow : FileItemGrid;
        let innerFiles;

        innerFiles = files.map(function(file) {
            return <ItemComponent
                isSelected={this.props.mainInfo.selectedIds.indexOf(file.id) !== -1}
                rightClick={this.rightClickFile}
                onDoubleClick={this.onDoubleClick}
                selectFile={this.selectFileByClick}
                key={file.id}
                file={file}
            />;
        }, this);

        if (isTableView) {
            innerFiles = (
                <table id="disk-table" className="table disk-table">
                    <tbody>
                        {innerFiles}
                    </tbody>
                </table>
            );
        }

        return (
            <div>
                {isTableView ? <FilesTableHead /> : ''}
                <div
                    id="filesContainer"
                    className={(isTableView ? '' : 'row container-fluid relative ') + 'scroll-files'}
                    onClick={this.leftClickOnFilesContainer}
                    onContextMenu={this.rightClickOnFilesContainer}
                >
                    {innerFiles}
                </div>
                <Footer
                    mainInfo={this.props.mainInfo}
                    driveActions={this.props.driveActions}
                    deleteSelectedFiles={this.deleteSelectedFiles}
                />
            </div>
        );
    }
}

export default FilesGrid;