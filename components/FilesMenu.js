import React from 'react';

const FilesMenu = (props) => {
    const isOneFile = props.selectedIds.length === 1,
        firstSelectedFileId = props.selectedIds[0];

    let viewLi, openViewByLinkLi, downloadLi, renameLi, createFolderLi, shareLi, editRightsLi, moveToLi, deleteLi, loadFileLi;
    if (isOneFile) {
        viewLi = <li><a href="#">Предварительный просмотр</a></li>;
        openViewByLinkLi = <li><a href="#">Включить доступ по ссылке</a></li>;
        if (props.files[firstSelectedFileId]['isFolder'] === false) {
            downloadLi = <li><a onClick={e => props.downloadFile(e)} href="#"><span className="glyphicon glyphicon-ok" ariaHidden="true" />Скачать</a></li>;
        }
        renameLi = <li><a onClick={e => props.openRenameModal(e)} href="#"><span className="glyphicon glyphicon-ok" ariaHidden="true" />Переименовать</a></li>;
    }

    if (props.selectedIds.length == 0) {
        createFolderLi = <li><a onClick={e => props.openCreateFolderModal(e)} href="#"><span className="glyphicon glyphicon-ok" ariaHidden="true" />Создать папку</a></li>;
        loadFileLi = <li><a onClick={e => props.openLoadFileModal()} href="#"><span className="glyphicon glyphicon-ok" ariaHidden="true" />Загрузить файл</a></li>;
    } else {
        shareLi = <li><a href="#">Расшарить</a></li>;
        editRightsLi = <li><a href="#">Редактирование прав</a></li>;
        moveToLi = <li><a onClick={e => props.openMoveToModal()} href="#">Переместить в</a></li>;
        deleteLi = <li><a href="#" onClick={e => {props.deleteFiles(e)}}><span className="glyphicon glyphicon-ok" ariaHidden="true" />Удалить</a></li>;
    }

    if (props.isShowFilesMenu) {
        return (
            <div className="dropdown open absolute" style={{top: props.posY, left: props.posX}}>
                <ul className="dropdown-menu">
                    {createFolderLi}
                    {loadFileLi}
                    {viewLi}
                    {openViewByLinkLi}
                    {shareLi}
                    {editRightsLi}
                    {moveToLi ? <li role="separator" className="divider" /> : ''}
                    {moveToLi}
                    {renameLi}
                    {downloadLi}
                    {deleteLi ? <li role="separator" className="divider" /> : ''}
                    {deleteLi}
                </ul>
            </div>
        );
    }

    return null;
};

FilesMenu.propTypes = {
    files: React.PropTypes.object.isRequired,
    posX: React.PropTypes.number.isRequired,
    posY: React.PropTypes.number.isRequired,
    deleteFiles: React.PropTypes.func.isRequired,
    downloadFile: React.PropTypes.func.isRequired,
    openRenameModal: React.PropTypes.func.isRequired,
    openMoveToModal: React.PropTypes.func.isRequired,
    openLoadFileModal: React.PropTypes.func.isRequired,
    openCreateFolderModal: React.PropTypes.func.isRequired,
    isShowFilesMenu: React.PropTypes.bool.isRequired
};

export default FilesMenu;