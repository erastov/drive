import React, {Component} from 'react';
import TopPanel from './TopPanel';
import FilesGrid from './FilesGrid';
import { bindActionCreators } from 'redux';
import * as driveActionsCreators from '../actions/DriveActions';
import { connect } from 'react-redux';
import listensToClickOutside from 'react-onclickoutside/decorator';


@connect(
    state => ({
        mainInfo: state.mainInfo
    }),
    dispatch => ({
        driveActions: bindActionCreators(driveActionsCreators, dispatch)
    })
)

@listensToClickOutside()

class App extends Component
{
    handleClickOutside = (event) => {
        this.props.driveActions.filesOutClick();
    };

    componentDidMount() {
        let id = document.getElementById('drive-container').getAttribute('data-id');
        this.props.driveActions.loadFolderData(id);
    }

    render() {
        const mainInfo = this.props.mainInfo;
        if (mainInfo.isLoading) {
            return (
                <div className="text-center">
                    <img src="/images/loading.gif" />
                </div>
            );
        }

        return (
            <div>
                <TopPanel
                    loadFolderData={this.props.driveActions.loadFolderData}
                    breadcrumbs={mainInfo.breadcrumbs}
                    viewType={mainInfo.viewType}
                    setViewType={this.props.driveActions.setViewType}
                />
                <FilesGrid
                    mainInfo={this.props.mainInfo}
                    driveActions={this.props.driveActions}
                />
            </div>
        );
    }
}

export default App;