import React from 'react';

export const FileItemGrid = props => {
    const file = props.file;
    let imageDiv = '';
    let containerClassName = 'col-xs-3 ';

    if (file.isFolder) {
        containerClassName += ' drive-grid__item_folder ';
    } else {
        containerClassName += ' drive-grid__item ';
        imageDiv = (
            <div className='drive-grid__item__image-container'>
                <img className="drive-grid__item__image" src={file.mimeType.indexOf('image') > -1 ? file.thumbnailLink : file.thumbnailLink} />
            </div>
        );
    }

    if (props.isSelected && file.isFolder) {
        containerClassName += ' drive-grid__item_selected '
    }

    if (file.isFolderLastFile && file.isFolder == false) {
        containerClassName += ' clear ';
    }

    return (
        <div onDoubleClick={e => props.onDoubleClick(e, file)} className={containerClassName} >
            <div className={props.isSelected && file.isFolder == false ? 'drive-grid__item_selected ' : ''}
                 onClick={e => props.selectFile(e, file.id)}
                 onContextMenu={e => props.rightClick(e, file.id)}>
                {imageDiv}
                <div className="drive-grid__item__name" title={file.title}>
                    <img src={file.iconLink} /> {file.title}
                </div>
            </div>
        </div>
    );
};

FileItemGrid.propTypes = {
    onDoubleClick: React.PropTypes.func.isRequired,
    rightClick: React.PropTypes.func.isRequired,
    file: React.PropTypes.object.isRequired,
    selectFile: React.PropTypes.func.isRequired,
    isSelected: React.PropTypes.bool.isRequired
};

export default FileItemGrid;