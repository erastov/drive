import React from 'react';

export const FileItemRow = props => {
    const file = props.file;
    return (
        <tr
            onClick={e => props.selectFile(e, file.id)}
            onDoubleClick={e => props.onDoubleClick(e, file)}
            onContextMenu={e => props.rightClick(e, file.id)}
            className={props.isSelected ? ' drive-grid__item_selected' : ''}>
            <td>
                <div className="table-grid-thumbnail"
                    style={{background: 'url(' + (file.mimeType.indexOf('image') > -1 ? file.thumbnailLink : file.iconLink) + ') 0 0 no-repeat'}}/>
                {file.title}
            </td>
            <td className='nowrap'>
                {file.fileSize}
            </td>
            <td className='nowrap'>{file.createdDate}</td>
            <td className={"file-properties-" + file.id}/>
        </tr>
    );
};

/*<td style={{whiteSpace: 'nowrap'}}>
    <a className="btn btn-success btn-xs j-edit-file-show-rights">Permissions</a>
    <a className="btn btn-primary btn-xs j-show-file-properties">Share</a>
</td>*/

FileItemRow.propTypes = {
    onDoubleClick: React.PropTypes.func.isRequired,
    rightClick: React.PropTypes.func.isRequired,
    file: React.PropTypes.object.isRequired,
    selectFile: React.PropTypes.func.isRequired,
    isSelected: React.PropTypes.bool.isRequired
};

export default FileItemRow;