import React from 'react';

export const FilesTableHead = props => {
    return (
        <table>
            <thead>
            <tr>
                <th>Name</th>
                <th>Size</th>
                <th>Created</th>
            </tr>
            </thead>
        </table>
    );
};

export default FilesTableHead;