import React, { Component } from 'react';

class MoveToModal extends Component{

    static propTypes = {
        isOpened: React.PropTypes.bool.isRequired,
        isCanSubmit: React.PropTypes.bool.isRequired,
        closeMoveToModal: React.PropTypes.func.isRequired,
        submitMoveTo: React.PropTypes.func.isRequired
    };

    renderTree(tree) {
        let treeView = '';
        for (let i in tree) {
            if (tree.hasOwnProperty(i)) {
                if (tree[i].children.length > 0) {
                    treeView
                } else {
                    
                }
            }
        }
    }

    render() {
        if (this.props.isOpened) {
            return (
                <div className="modal fade in" style={{display: 'block'}}>
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button onClick={e => this.props.closeMoveToModal(e)} type="button" className="close"><span>&times;</span></button>
                                <h4 className="modal-title">Переместить в</h4>
                            </div>
                            <div className="modal-body">
                                Дерево папок с выбором
                            </div>
                            <div className="modal-footer">
                                <button onClick={e => this.props.closeMoveToModal(e)} type="button" className="btn btn-default">Отмена</button>
                                <button onClick={e => this.props.submitNewFile(e)} type="button" className="btn btn-primary">Переместить</button>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        return null;
    }
}

export default MoveToModal;