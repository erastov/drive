import React from 'react';

const RenameModal = (props) => {
    if (props.isOpened) {
        return (
            <div className="modal fade in" style={{display: 'block'}}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button onClick={e => props.closeRenameModal(e)} type="button" className="close"><span>&times;</span></button>
                            <h4 className="modal-title">Переименовать</h4>
                        </div>
                        <div className="modal-body">
                            <p>Введите новое название</p>
                            <form onSubmit={e => props.submitFileName(e)}>
                                <div className="form-group">
                                    <input autoFocus type="text" onChange={e => props.changeFileName(e)} className="form-control" value={props.name} />
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button onClick={e => props.closeRenameModal(e)} type="button" className="btn btn-default">Отмена</button>
                            <button onClick={e => props.submitFileName(e)} type="button" className="btn btn-primary">Ок</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    return null;
};

RenameModal.propTypes = {
    isOpened: React.PropTypes.bool.isRequired,
    isCanSubmit: React.PropTypes.bool.isRequired,
    name: React.PropTypes.string.isRequired,
    changeFileName: React.PropTypes.func.isRequired,
    closeRenameModal: React.PropTypes.func.isRequired,
    submitFileName: React.PropTypes.func.isRequired
};

export default RenameModal;