import React from 'react';

const LoadFileModal = (props) => {
    if (props.isOpened) {

        let footer;
        if (props.isUploading) {
            footer = (
                <div className="modal-body">
                    <div className="text-center">
                        <img src="/images/loading.gif" />
                    </div>
                </div>
            );
        } else {
            footer = (
                <div>
                    <div className="modal-body">
                        <form onSubmit={e => props.uploadFile(e)}>
                            <div className="form-group">
                                <input id="UploadDriveFile" type="file" className="form-control"/>
                            </div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button onClick={e => props.closeLoadFileModal(e)} type="button" className="btn btn-default">Отмена</button>
                        <button onClick={e => props.uploadFile(e)} type="button" className="btn btn-primary">Загрузить</button>
                    </div>
                </div>
            );
        }

        return (
            <div className="modal fade in" style={{display: 'block'}}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button onClick={e => props.closeLoadFileModal(e)} type="button" className="close"><span>&times;</span></button>
                            <h4 className="modal-title">Загрузка файла</h4>
                        </div>
                        {footer}
                    </div>
                </div>
            </div>
        );
    }

    return null;
};

LoadFileModal.propTypes = {
    isOpened: React.PropTypes.bool.isRequired,
    isUploading: React.PropTypes.bool.isRequired,
    isCanSubmit: React.PropTypes.bool.isRequired,
    closeLoadFileModal: React.PropTypes.func.isRequired,
    uploadFile: React.PropTypes.func.isRequired
};

export default LoadFileModal;