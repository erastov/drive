import React from 'react';

const RenameModal = (props) => {
    if (props.isOpened) {
        return (
            <div className="modal fade in" style={{display: 'block'}}>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button onClick={e => props.closeCreateFolderModal(e)} type="button" className="close"><span>&times;</span></button>
                            <h4 className="modal-title">Новая папка</h4>
                        </div>
                        <div className="modal-body">
                            <form onSubmit={e => props.submitNewFolder(e)}>
                                <div className="form-group">
                                    <input autoFocus type="text" onChange={e => props.changeNewFolderName(e)} className="form-control" value={props.name} />
                                </div>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button onClick={e => props.closeCreateFolderModal(e)} type="button" className="btn btn-default">Отмена</button>
                            <button onClick={e => props.submitNewFolder(e)} type="button" className="btn btn-primary">Создать</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    return null;
};

RenameModal.propTypes = {
    isOpened: React.PropTypes.bool.isRequired,
    isCanSubmit: React.PropTypes.bool.isRequired,
    name: React.PropTypes.string.isRequired,
    changeNewFolderName: React.PropTypes.func.isRequired,
    closeCreateFolderModal: React.PropTypes.func.isRequired,
    submitNewFolder: React.PropTypes.func.isRequired
};

export default RenameModal;