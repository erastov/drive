import React from 'react';

function getNextViewType(viewType) {
    return viewType === 'table' ? 'grid' : 'table';
}

const TopPanel = (props) => {
    const iconClass = props.viewType === 'table' ? 'glyphicon-th-large' : 'glyphicon-th-list';
    return (
        <div className="row">
            <div className="col-xs-12">
                <ul className="breadcrumb breadcrumb-drive">
                    {props.breadcrumbs.map(function(folder) {
                        return <li key={folder.id}><i><a onClick={e => {e.preventDefault(); props.loadFolderData(folder.id)}} href="/drive/drive/index">{folder.label}</a></i></li>;
                    })}
                    <li className="pull-right">
                        <a href="#" onClick={e => props.setViewType(getNextViewType(props.viewType))}>
                            <span className={'glyphicon ' + iconClass} aria-hidden="true" />
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    );
};

TopPanel.PropTypes = {
    viewType: React.PropTypes.string.isRequired,
    setViewType: React.PropTypes.func.isRequired,
    loadFolderData: React.PropTypes.func.isRequired,
    breadcrumbs: React.PropTypes.array.isRequired
};

export default TopPanel;