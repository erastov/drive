import React, { Component } from 'react';

import RenameModal from './modals/RenameModal';
import CreateFolderModal from './modals/CreateFolderModal';
import MoveToModal from './modals/MoveToModal';
import LoadFileModal from './modals/LoadFileModal';
import FilesMenu from './FilesMenu';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as renameModalActionsCreators from '../actions/RenameModalActions';
import * as createFolderActionsCreators from '../actions/CreateFolderActions';
import * as moveToActionsCreators from '../actions/MoveToModalActions';
import * as loadFileActionsCreators from '../actions/LoadFilelActions';

@connect(
    state => ({
        renameModal: state.renameModal,
        createFolderModal: state.createFolderModal,
        moveToModal: state.moveToModal,
        loadFileModal: state.loadFileModal
    }),
    dispatch => ({
        renameModalActions: bindActionCreators(renameModalActionsCreators, dispatch),
        createFolderModalActions: bindActionCreators(createFolderActionsCreators, dispatch),
        moveToActions: bindActionCreators(moveToActionsCreators, dispatch),
        loadFileActions: bindActionCreators(loadFileActionsCreators, dispatch)
    })
)

class Footer extends Component {

    uploadFile = (e) => {
        let formData = new FormData();
        const input = document.getElementById('UploadDriveFile');

        if (input.value) {
            formData.append('folderId', this.props.mainInfo.parentId);
            formData.append('file', input.files[0]);
            this.props.loadFileActions.uploadFile(formData);
        }
    };

    downloadFile = e => {
        e.preventDefault();
        window.open('https://docs.google.com/uc?id=' + this.props.mainInfo.selectedIds[0] + '&export=download');
        this.props.driveActions.closeFileMenu();
    };

    openRenameModal = (e) => {
        e.preventDefault();
        let file = this.props.mainInfo.files[ this.props.mainInfo.selectedIds[0] ];
        this.props.renameModalActions.openRenameModal(file.title);
    };

    submitFileName = (e) => {
        e.preventDefault();
        let data = new FormData();
        data.append('title', this.props.renameModal.name);
        data.append('id', this.props.mainInfo.selectedIds[0]);
        this.props.renameModalActions.submitRename(data, this.props.selectedIds[0]);
    };

    submitNewFolder = (e) => {
        e.preventDefault();
        let data = new FormData();
        data.append('title', this.props.createFolderModal.name);
        data.append('parentId', this.props.mainInfo.parentId);
        this.props.createFolderModalActions.submitNewFolder(data);
    };

    changeFileName = (e) => {
        e.preventDefault();
        this.props.renameModalActions.changeFileName(e.target.value);
    };

    changeNewFolderName = (e) => {
        e.preventDefault();
        this.props.createFolderModalActions.changeFolderName(e.target.value);
    };

    render() {
        return (
            <div>
                <FilesMenu
                    selectedIds={this.props.mainInfo.selectedIds}
                    posX={this.props.mainInfo.posX}
                    posY={this.props.mainInfo.posY}
                    downloadFile={this.downloadFile}
                    files={this.props.mainInfo.files}
                    deleteFiles={this.props.deleteSelectedFiles}
                    openRenameModal={this.openRenameModal}
                    openMoveToModal={this.props.moveToActions.getFoldersTree}
                    openCreateFolderModal={this.props.createFolderModalActions.openCreateFolderModal}
                    openLoadFileModal={this.props.loadFileActions.openLoadFileModal}
                    isShowFilesMenu={this.props.mainInfo.isShowFilesMenu}
                />

                <RenameModal
                    {...this.props.renameModal}
                    {...this.props.renameModalActions}
                    changeFileName={this.changeFileName}
                    submitFileName={this.submitFileName}
                />

                <CreateFolderModal
                    {...this.props.createFolderModal}
                    {...this.props.createFolderModalActions}
                    changeNewFolderName={this.changeNewFolderName}
                    submitNewFolder={this.submitNewFolder}
                />

                <MoveToModal
                    {...this.props.moveToModal}
                    {...this.props.moveToActions}
                />

                <LoadFileModal
                    {...this.props.loadFileModal}
                    {...this.props.loadFileActions}
                    uploadFile={this.uploadFile}
                />
            </div>
        );
    }
}

export default Footer;