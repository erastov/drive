import * as types from '../constants/RenameModalActions';

export const changeFileName = (name) => ({
    type: types.CHANGE_FILE_NAME,
    name
});

export const openRenameModal = (name) => ({
    type: types.OPEN_RENAME_MODAL,
    name
});

export const closeRenameModal = () => ({
    type: types.CLOSE_RENAME_MODAL
});

export const applyNewFileName = (name, fileId) => ({
    type: types.APPLY_NEW_FILE_NAME,
    name,
    fileId
});

export function submitRename(data, fileId) {
    return dispatch => {
        return fetch('/drive/files/rename', {
            credentials: 'same-origin',
            method: 'POST',
            body: data
        })
        .then(response => response.json())
        .then(response => {
            if (response.errors.length) {

            } else {

                dispatch(applyNewFileName(response.name, fileId));
            }
        });
    }
}