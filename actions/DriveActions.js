import * as types from '../constants/Actions';

export const changeToggleAll = () => ({
    type: types.TOGGLE_ALL_FILES
});

export const switchViewType = (viewType) => ({
    type: types.TOGGLE_VIEW_TYPE,
    isShowFilesMenu: false,
    viewType: viewType
});

export const filesOutClick = () => ({
    type: types.FILES_OUTCLICK,
    isShowFilesMenu: false
});

export const selectFile = (id) => ({
    type: types.SELECT_FILE_LEFT_CLICK,
    isShowFilesMenu: false,
    selectedIds: [id],
    id
});

export const toggleFileMenu = (show, posX, posY) => ({
    type: types.TOGGLE_FILE_MENU,
    isShowFilesMenu: show,
    posX,
    posY
});

export const addFileToSelected = (id) => ({
    type: types.SELECT_FILE_LEFT_CLICK_WITH_CTRL,
    isShowFilesMenu: false,
    id
});

export const setSelectedIds = (ids) => ({
    type: types.SET_SELECTED_IDS,
    ids
});

export const removeFileFromSelected = (id) => ({
    type: types.REMOVE_FILE_FROM_SELECTED,
    id
});

export const setInitialData = response => ({
    type: types.SET_INITIAL_DATA,
    isToggleAll: false,
    viewType: response.viewType,
    files: response.files,
    breadcrumbs: response.breadcrumbs,
    selectedIds: [],
    isLoading: false,
    parentId: response.parentId
});

export function loadFolderData(folderId = '') {
    return dispatch => {
        dispatch(setLoading(true));
        return fetch('/drive/drive/info?id=' + folderId, {
            credentials: 'same-origin'
        })
        .then(response => response.json())
        .then(response => {
            history.pushState(null, '', '/drive/drive/react?id=' + folderId);
            return response;
        })
        .then(response => {
            dispatch(setInitialData(response));
            return response;
        })
        .then(response => dispatch(setLoading(false)));
    }
}

export const closeFileMenu = () => ({
    type: types.CLOSE_FILE_MENU
});

export const deleteFilesFromList = ids => ({
    type: types.DELETE_FILES,
    ids
});

export const setLoading = (isLoading) => ({
    type: types.LOADING,
    isLoading
});

export const closeAllModalsAndMenu = () => ({
    type: types.CLOSE_ALL_MODALS_AND_MENU
});

export function setViewType(viewType) {
    let data = new FormData();
    data.append('viewType', viewType);
    return dispatch => {
        dispatch(switchViewType(viewType));
        return fetch('/drive/drive/set-view', {
            credentials: 'same-origin',
            method: 'POST',
            body: data
        });
    }
}

export function deleteSelectedFiles(data) {
    return dispatch => {
        return fetch('/drive/files/delete', {
            credentials: 'same-origin',
            method: 'POST',
            body: data
        })
        .then(response => response.json())
        .then(response => {
            if (response.hasOwnProperty('errors') && response.errors.length) {

            } else {
                dispatch(deleteFilesFromList(response.ids));
            }
        });
    }
}