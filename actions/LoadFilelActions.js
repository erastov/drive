import * as types from '../constants/LoadFileActions';

export const openLoadFileModal = () => ({
    type: types.OPEN_LOAD_FILE_MODAL
});

export const closeLoadFileModal = () => ({
    type: types.CLOSE_LOAD_FILE_MODAL
});

export const setUploading = () => ({
    type: types.UPLOADING
});

export const addLoadedFile = (file) => ({
    type: types.NEW_FILE_LOADED,
    file
});

export function uploadFile(data) {
    return dispatch => {
        dispatch(setUploading());
        return fetch('/drive/files/load', {
            credentials: 'same-origin',
            method: 'POST',
            body: data
        })
        .then(response => response.json())
        .then(response => {
            if (response.errors.length) {

            } else {
                dispatch(addLoadedFile(response.file));
            }
        });
    }
}