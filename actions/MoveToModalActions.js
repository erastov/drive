import * as types from '../constants/MoveToActions';

export const openMoveToModal = () => ({
    type: types.OPEN_MOVE_TO_MODAL
});

export const closeMoveToModal = () => ({
    type: types.CLOSE_MOVE_TO_MODAL
});

export const removeFiles = ids => ({
    type: types.REMOVE_MOVED_FILES,
    ids
});

export const setFoldersTree = (parentId, folders) => ({
    type: types.SET_ROOT_FOLDER_CHILDREN,
    parentId,
    folders
});

export function getFoldersTree(id = '') {
    return dispatch => {
        return fetch('/drive/files/get-folders-tree?id=' + id, {
            credentials: 'same-origin',
            method: 'GET'
        })
        .then(response => response.json())
        .then(response => dispatch(setFoldersTree(response.parentId, response.folders)))
        .then(() => dispatch(openMoveToModal()))
    }
}