import * as types from '../constants/CreateFolderActions';

export const changeFolderName = (name) => ({
    type: types.CHANGE_FOLDER_NAME,
    name
});

export const openCreateFolderModal = () => ({
    type: types.OPEN_CREATE_FOLDER_MODAL,
    name
});

export const closeCreateFolderModal = () => ({
    type: types.CLOSE_CREATE_FOLDER_MODAL
});

export const addNewFolder = (file) => ({
    type: types.ADD_NEW_FOLDER,
    file
});

export function submitNewFolder(data) {
    return dispatch => {
        return fetch('/drive/files/create-folder', {
            credentials: 'same-origin',
            method: 'POST',
            body: data
        })
        .then(response => response.json())
        .then(response => {
            if (response.errors.length) {

            } else {
                dispatch(addNewFolder(response.file));
            }
        });
    }
}