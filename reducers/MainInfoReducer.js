import * as types from '../constants/Actions';
import * as renameTypes from '../constants/RenameModalActions';
import * as moveTyTypes from '../constants/MoveToActions';
import * as createFolderTypes from '../constants/CreateFolderActions';
import * as loadFileTypes from '../constants/LoadFileActions';

const initialState = {
    isToggleAll: false,
    viewType: 'grid',
    files: [],
    selectedIds: [],
    isLoading: true,
    breadcrumbs: 'My drive',
    isShowFilesMenu: false,
    posX: 0,
    posY: 0,
    lastSelectedFileId: null,
    isFileFocus: false,
    parentId: ''
};

export const MainInfoReducer = (state = initialState, action) => {
    let files;
    let newFiles;

    switch (action.type) {
        case types.CLOSE_FILE_MENU:
            return {
                ...state,
                isShowFilesMenu: false
            };
        case types.TOGGLE_ALL_FILES:
            return {
                ...state,
                isToggleAll: !state.isToggleAll
            };
        case types.SET_INITIAL_DATA:
            const { type, ...data } = action;
            return {
                ...state,
                ...data
            };
        case types.DELETE_FILES:
            newFiles = {};
            for (let id in state.files) {
                if (state.files.hasOwnProperty(id)) {
                    if (action.ids.hasOwnProperty(id) === false) {
                        newFiles[id] = state.files[id];
                    }
                }
            }
            return {
                ...state,
                files: newFiles,
                isShowFilesMenu: false,
                lastSelectedFileId: null,
                selectedIds: []
            };
        case types.TOGGLE_FILE_MENU:
            return {
                ...state,
                isShowFilesMenu: action.isShowFilesMenu,
                posX: action.posX,
                posY: action.posY
            };
        case types.TOGGLE_VIEW_TYPE:
            return {
                ...state,
                isShowFilesMenu: action.isShowFilesMenu,
                viewType: action.viewType
            };
        case types.SET_VIEW_FOCUS:
            return {
                ...state,
                isFileFocus: action.isFileFocus
            };
        case types.SELECT_FILE_LEFT_CLICK_WITH_CTRL:
            return {
                ...state,
                selectedIds: [...state.selectedIds, action.id],
                isShowFilesMenu: action.isShowFilesMenu,
                lastSelectedFileId: action.id,
                isFileFocus: true
            };
        case types.SET_SELECTED_IDS:
            return {
                ...state,
                selectedIds: action.ids,
                isFileFocus: true
            };
        case types.REMOVE_FILE_FROM_SELECTED:
            var selectedIds = state.selectedIds.filter(e => e != action.id);
            return {
                ...state,
                selectedIds: selectedIds,
                isFileFocus: true
            };
        case types.LOADING:
            return {
                ...state,
                isLoading: action.isLoading
            };
        case types.FILES_OUTCLICK:
            return {
                ...state,
                selectedIds: [],
                isShowFilesMenu: false,
                isFileFocus: false
            };
        case types.SELECT_FILE_LEFT_CLICK:
            return {
                ...state,
                selectedIds: action.selectedIds,
                isShowFilesMenu: action.isShowFilesMenu,
                lastSelectedFileId: action.id,
                isFileFocus: true
            };
        case renameTypes.OPEN_RENAME_MODAL:
            return {
                ...state,
                isShowFilesMenu: false
            };
        case moveTyTypes.OPEN_MOVE_TO_MODAL:
            return {
                ...state,
                isShowFilesMenu: false
            };
        case createFolderTypes.OPEN_CREATE_FOLDER_MODAL:
            return {
                ...state,
                isShowFilesMenu: false
            };
        case loadFileTypes.OPEN_LOAD_FILE_MODAL:
            return {
                ...state,
                isShowFilesMenu: false
            };
        case loadFileTypes.NEW_FILE_LOADED:
            newFiles = {};
            files = [];

            const keys = Object.keys(state.files);
            if (keys.length === 0) {
                files.push(action.file);
            } else {
                for (let i in state.files) {
                    if (state.files.hasOwnProperty(i)) {
                        files.push(state.files[i]);
                    }
                }
                console.log('********************************************************');
                console.log(files);

                if (files[0].isFolder) {
                    let filesCount = files.length - 1;
                    for (let i = 0; i < filesCount; i++ ) {
                        if (files[i].isFolder === true && files[i+1].isFolder === false) {
                            console.log('files[i].isFolder === true && files[i+1].isFolder === false');
                            files = [].concat(files.slice(0, i + 1), [action.file], files.slice(i + 1, filesCount + 1));
                            break;
                        } else {
                            console.log('((');
                        }
                    }

                    if ((files.length - 1) === filesCount) {
                        files.push(action.file);
                    }
                } else {
                    files.unshift(action.file);
                }
            }

            let fileId;
            for (let i in files) {
                fileId = files[i]['id'];
                newFiles[fileId] = files[i];
            }
            console.log(newFiles);
            return {
                ...state,
                files: newFiles
            };
        case createFolderTypes.ADD_NEW_FOLDER:
            return {
                ...state,
                files: {[action.file.id]: action.file, ...state.files}
            };
        case renameTypes.APPLY_NEW_FILE_NAME:
            files = {};
            for (let id in state.files) {
                files[id] = state.files[id];
                if (id == action.fileId) {
                    files[id]['title'] = action.name
                }
            }

            return {
                ...state,
                files: files
            };
        case types.CLOSE_ALL_MODALS_AND_MENU:
            return {
                ...state,
                isShowFilesMenu: false
            };
        default:
            return state;
    }
};

export default MainInfoReducer;