import * as types from '../constants/MoveToActions';
import * as mainInfoTypes from '../constants/Actions';

const initialState = {
    isOpened: false,
    parentId: '',
    filesIds: '',
    folders: []
};

const MoveToModalReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.OPEN_MOVE_TO_MODAL:
            return {
                ...state,
                isOpened: true
            };
        case types.CLOSE_MOVE_TO_MODAL:
            return {
                ...state,
                isOpened: false
            };
        case mainInfoTypes.CLOSE_ALL_MODALS_AND_MENU:
            return {
                ...state,
                isOpened: false
            };
        case types.SET_ROOT_FOLDER_CHILDREN:
            console.log(action);
            return {
                ...state
            };
        default:
            return state;
    }
};

export default MoveToModalReducer;