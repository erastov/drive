import * as types from '../constants/CreateFolderActions';
import * as mainInfoTypes from '../constants/Actions';

const initialState = {
    isOpened: false,
    isCanSubmit: false,
    name: ''
};

const CreateFolderModalReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.CHANGE_FOLDER_NAME:
            return {
                ...state,
                isCanSubmit: action.name.length > 0,
                name: action.name
            };
        case types.CLOSE_CREATE_FOLDER_MODAL:
            return {
                ...state,
                isCanSubmit: false,
                isOpened: false
            };
        case types.OPEN_CREATE_FOLDER_MODAL:
            return {
                ...state,
                name: 'Без названия',
                isOpened: true,
                isCanSubmit: true
            };
        case types.ADD_NEW_FOLDER:
            return {
                ...state,
                isOpened: false
            };
        case mainInfoTypes.CLOSE_ALL_MODALS_AND_MENU:
            return {
                ...state,
                isOpened: false
            };
        default:
            return state;
    }
};


export default CreateFolderModalReducer;