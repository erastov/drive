import * as types from '../constants/RenameModalActions';
import * as mainInfoTypes from '../constants/Actions';

const initialState = {
    isOpened: false,
    isCanSubmit: false,
    name: ''
};

const RenameModalReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.CHANGE_FILE_NAME:
            return {
                ...state,
                isCanSubmit: action.name.length > 0,
                name: action.name
            };
        case types.CLOSE_RENAME_MODAL:
            return {
                ...state,
                name: '',
                isCanSubmit: false,
                isOpened: false
            };
        case types.OPEN_RENAME_MODAL:
            return {
                ...state,
                name: action.name,
                isOpened: true,
                isCanSubmit: true
            };
        case types.APPLY_NEW_FILE_NAME:
            return {
                ...state,
                name: '',
                isCanSubmit: false,
                isOpened: false
            };
        case mainInfoTypes.CLOSE_ALL_MODALS_AND_MENU:
            return {
                ...state,
                isOpened: false
            };
        default:
            return state;
    }
};


export default RenameModalReducer;