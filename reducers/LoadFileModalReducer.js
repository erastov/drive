import * as types from '../constants/LoadFileActions';
import * as mainInfoTypes from '../constants/Actions';

const initialState = {
    isOpened: false,
    parentId: '',
    isCanSubmit: false,
    isUploading: false,
    error: ''
};

const LoadFileModalReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.OPEN_LOAD_FILE_MODAL:
            return {
                ...state,
                isOpened: true
            };
        case types.CLOSE_LOAD_FILE_MODAL:
            return {
                ...state,
                isOpened: false
            };
        case types.UPLOADING:
            return {
                ...state,
                isUploading: true
            };
        case types.NEW_FILE_LOADED:
            return {
                ...state,
                isUploading: false,
                isOpened: false
            };
        case mainInfoTypes.CLOSE_ALL_MODALS_AND_MENU:
            return {
                ...state,
                isOpened: false
            };
        default:
            return state;
    }
};

export default LoadFileModalReducer;