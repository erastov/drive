import { combineReducers } from 'redux';
import MainInfoReducer from './MainInfoReducer';
import MoveToModalReducer from './MoveToModalReducer';
import RenameModalReducer from './RenameModalReducer';
import CreateFolderModalReducer from './CreateFolderModalReducer';
import LoadFileModalReducer from './LoadFileModalReducer';

export default combineReducers({
    mainInfo: MainInfoReducer,
    moveToModal: MoveToModalReducer,
    renameModal: RenameModalReducer,
    createFolderModal: CreateFolderModalReducer,
    loadFileModal: LoadFileModalReducer
});